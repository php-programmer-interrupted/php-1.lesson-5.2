<?php
    // Каталоги сайта
    define('SITE_DIR', __DIR__ . '/../');               // Выход на один уровень вверх в структуре каталогов сайта
    define('CONFIG_DIR', SITE_DIR . 'config/');         // Конфигурация
    define('DATA_DIR', SITE_DIR . 'data/');             // Данные
    define('ENGINE_DIR', SITE_DIR . 'engine/');         // Функционал
    define('WWW_DIR', SITE_DIR . 'public/');            // Каталог, доступный посетителям сайта
    define('TEMPLATES_DIR', SITE_DIR . 'templates/');   // Шаблоны
    define('LOG_DIR', SITE_DIR . 'logs/');				// Логи
    define('IMG_DIR', 'img/');                          // Изображения
    
	// Константы соединения с б/д
	define('DB_HOST', 'ls');
	define('DB_USER', 'geekbrains');
	define('DB_PASS', 'opdf117!');
	define('DB_NAME', 'geekbrains');

    // Подключение функционала
    require_once ENGINE_DIR . 'functions.php';
    require_once ENGINE_DIR . 'db.php';
    require_once ENGINE_DIR . 'news.php';
